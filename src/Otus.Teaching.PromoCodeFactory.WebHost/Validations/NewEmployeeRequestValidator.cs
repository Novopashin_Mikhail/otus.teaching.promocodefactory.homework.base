using FluentValidation;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Validations
{
    public class NewEmployeeRequestValidator : AbstractValidator<NewEmployeeRequest>
    {
        public NewEmployeeRequestValidator()
        {
            RuleFor(employee => employee).NotNull().WithMessage("Необходимо заполнить объект");
            RuleFor(employee => employee.FirstName).NotNull().WithMessage("Необходимо заполнить поле {PropertyName}");
            RuleFor(employee => employee.LastName).NotNull().WithMessage("Необходимо заполнить поле {PropertyName}");
            RuleFor(employee => employee.Email).NotNull().WithMessage("Необходимо заполнить поле {PropertyName}");
            RuleFor(employee => employee.Roles).NotNull().WithMessage("Необходимо заполнить поле {PropertyName}");
            RuleFor(employee => employee.Roles).NotEmpty().WithMessage("Необходимо заполнить поле {PropertyName}");
            RuleFor(employee => employee.AppliedPromocodesCount).GreaterThanOrEqualTo(0).WithMessage("Поле {PropertyName} должно быть больше или равно 0, текущее значение {PropertyValue}");
        }
    }
}